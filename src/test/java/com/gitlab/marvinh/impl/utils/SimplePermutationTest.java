package com.gitlab.marvinh.impl.utils;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.exceptions.PermutationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.KeyException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Created by chief on 27.05.17.
 */
class SimplePermutationTest {
    Permutation<Integer, String> permutation;

    @BeforeEach
    void setUp() {
        Map<Integer, String> elements = new HashMap<>();
        elements.put(0, "a");
        elements.put(1, "f");
        elements.put(10, "xz");
        permutation = new SimplePermutation<>(elements);
    }

    @Test
    void getElement() throws KeyException {
        assertEquals("a", permutation.getElement(0));
        assertEquals("xz", permutation.getElement(10));
        assertEquals("f", permutation.getElement(1));
        assertThrows(PermutationException.class, () -> permutation.getElement(2));
    }

    @Test
    void getColumns() {
        assertEquals(new HashSet<>(Arrays.asList(
                0,
                10,
                1
        )), new HashSet<>(permutation.getColumns()));
    }

}