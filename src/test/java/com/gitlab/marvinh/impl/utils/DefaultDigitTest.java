package com.gitlab.marvinh.impl.utils;

import com.gitlab.marvinh.api.Digit;
import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.impl.utils.DefaultDigit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by chief on 27.05.17.
 */
class DefaultDigitTest {
    private ValueDomain<String> valueDomain = mock(ValueDomain.class);
    private Digit<Integer, String> digit;

    @BeforeEach
    void setUp() {
        Collection<String> collection = new ArrayList<>(Arrays.asList(
                "a",
                "b",
                "c",
                "d",
                "e",
                "f"
        ));
        when(valueDomain.iterator()).thenReturn(collection.iterator(), collection.iterator());
        digit = new DefaultDigit<>(0, valueDomain);
    }

    @Test
    void getColumn() {
        assertEquals(0, Math.toIntExact(digit.getColumn()));
    }

    @Test
    void reset() {
        assertEquals("a", digit.current());
        assertEquals("b", digit.next());
        assertEquals("c", digit.next());
        digit.reset();
        assertEquals("a", digit.current());
        assertEquals("b", digit.next());
        assertEquals("c", digit.next());
        assertEquals("d", digit.next());
        assertEquals("e", digit.next());
        assertEquals("f", digit.next());
    }

    @Test
    void current() {
        assertEquals("a", digit.current());
        assertEquals("a", digit.current());
        digit.next();
        assertEquals("b", digit.current());
        assertEquals("b", digit.current());
        digit.next();
        assertEquals("c", digit.current());
        assertEquals("c", digit.current());
        digit.next();
        assertEquals("d", digit.current());
        assertEquals("d", digit.current());
        digit.next();
        assertEquals("e", digit.current());
        assertEquals("e", digit.current());
        digit.next();
        assertEquals("f", digit.current());
        assertEquals("f", digit.current());
    }

    @Test
    void hasNext() {
        for (int a = 0 ; a < 5; a++){
            assertTrue(digit.hasNext());
            digit.next();
        }
        assertFalse(digit.hasNext());
    }

    @Test
    void next() {
        assertEquals("b", digit.next());
        assertEquals("c", digit.next());
        assertEquals("d", digit.next());
        assertEquals("e", digit.next());
        assertEquals("f", digit.next());
    }

}