package com.gitlab.marvinh.impl.valuedomains;

import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.impl.valuedomains.SimpleValueDomain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 27.05.17.
 */
class SimpleValueDomainTest {
    ValueDomain<String> valueDomain;

    @BeforeEach
    void setUp() {
        Collection<String> strings = new HashSet<>(Arrays.asList(
                "a",
                "xz",
                "c",
                "e"
        ));
        valueDomain = new SimpleValueDomain<>(strings);
    }

    @Test
    void size() {
        assertEquals(4, valueDomain.size());
    }

    @Test
    void isEmpty() {
        assertFalse(valueDomain.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(valueDomain.contains("a"));
        assertFalse(valueDomain.contains("b"));
        assertTrue(valueDomain.contains("xz"));
        assertTrue(valueDomain.contains("c"));
        assertTrue(valueDomain.contains("e"));
    }

    @Test
    void iterator() {
        assertNotNull(valueDomain.iterator());
    }

    @Test
    void toArray() {
        Object[] result = valueDomain.toArray();
        assertEquals(new HashSet<>(Arrays.asList(
                "a",
                "xz",
                "c",
                "e"
        )), new HashSet<>(Arrays.asList(result)));
    }

    @Test
    void toArray2() {
        String[] result = new String[4];
        valueDomain.toArray(result);
        assertEquals(new HashSet<>(Arrays.asList(
                "a",
                "xz",
                "c",
                "e"
        )), new HashSet<>(Arrays.asList(result)));
    }

    @Test
    void containsAll() {
        assertTrue(valueDomain.containsAll(Arrays.asList(
                "a",
                "xz",
                "c",
                "e"
        )));
        assertFalse(valueDomain.containsAll(Arrays.asList(
                "a",
                "b",
                "xz",
                "c",
                "e",
                "f"
        )));
    }

}