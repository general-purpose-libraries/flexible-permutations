package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.impl.valuedomains.SimpleValueDomain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 27.05.17.
 */
class FixedSizePermutationGeneratorTest {
    private FixedSizePermutationGenerator<Integer, String> permutationGenerator;
    private ValueDomain<String> valueDomain1;

    @BeforeEach
    void setUp() {
        Collection<String> collection1 = new ArrayList<>(Arrays.asList(
                "a",
                "b",
                "c",
                "d",
                "e",
                "f"
        ));
        Collection<String> collection2 = new ArrayList<>(Arrays.asList(
                "0",
                "1",
                "2",
                "3"
        ));

        valueDomain1 = new SimpleValueDomain<>(collection1);
        ValueDomain<String> valueDomain2 = new SimpleValueDomain<>(collection2);

        Map<Integer, ValueDomain<String>> valueDomainMap = new HashMap<>();
        valueDomainMap.put(0, valueDomain1);
        valueDomainMap.put(1, valueDomain2);
        permutationGenerator = new FixedSizePermutationGenerator<>(valueDomainMap);
    }

    @Test
    void hasNext() {
        for (int a = 0; a < 24; a++){
            assertTrue(permutationGenerator.hasNext());
            permutationGenerator.next();
        }
        assertFalse(permutationGenerator.hasNext());
    }

    @Test
    void next() {
        Collection<List<String>> result = new HashSet<>();
        Collection<List<String>> expected = new HashSet<>();
        for (String string : valueDomain1){
            expected.add(Arrays.asList(string, "0"));
            expected.add(Arrays.asList(string, "1"));
            expected.add(Arrays.asList(string, "2"));
            expected.add(Arrays.asList(string, "3"));
        }
        while (permutationGenerator.hasNext()){
            Permutation<Integer, String> permutation = permutationGenerator.next();
            List<String> strings = new ArrayList<>(2);
            strings.add(permutation.getElement(0));
            strings.add(permutation.getElement(1));
            result.add(strings);
        }
        assertEquals(expected, result);
    }

}