package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.api.rules.IfThenNotRule;
import com.gitlab.marvinh.api.rules.IfThenRule;
import com.gitlab.marvinh.api.rules.Rule;
import com.gitlab.marvinh.api.rules.misc.RuleEntry;
import com.gitlab.marvinh.impl.valuedomains.SimpleValueDomain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by chief on 21.06.17.
 */
class AbstractPermutationGeneratorTest {
    private AbstractPermutationGenerator<Integer, String> permutationGenerator;
    private ValueDomain<String> valueDomain1;

    @BeforeEach
    void setUp() {
        Collection<String> collection1 = new ArrayList<>(Arrays.asList(
                "a",
                "b",
                "c",
                "d",
                "e",
                "f"
        ));
        Collection<String> collection2 = new ArrayList<>(Arrays.asList(
                "0",
                "1",
                "2",
                "3"
        ));

        Collection<Rule<Integer, String>> rules = Arrays.asList(
                new IfThenRule<>(
                        new RuleEntry<>(0, "a"),
                        new RuleEntry<>(1, "0")),
                new IfThenNotRule<>(
                        new RuleEntry<>(1, "2"),
                        new RuleEntry<>(0, "f")
                )
        );

        valueDomain1 = new SimpleValueDomain<>(collection1);
        ValueDomain<String> valueDomain2 = new SimpleValueDomain<>(collection2);

        Map<Integer, ValueDomain<String>> valueDomainMap = new HashMap<>();
        valueDomainMap.put(0, valueDomain1);
        valueDomainMap.put(1, valueDomain2);
        permutationGenerator = new FixedSizePermutationGenerator<>(valueDomainMap, rules);
    }

    @Test
    void hasNext() {
        for (int a = 0; a < 20; a++){
            assertTrue(permutationGenerator.hasNext());
            permutationGenerator.next();
        }
        assertFalse(permutationGenerator.hasNext());
    }

    @Test
    void next() {
        Collection<List<String>> result = new HashSet<>();
        Collection<List<String>> expected = new HashSet<>();
        for (String string : valueDomain1){
            expected.add(Arrays.asList(string, "0"));
            expected.add(Arrays.asList(string, "1"));
            expected.add(Arrays.asList(string, "2"));
            expected.add(Arrays.asList(string, "3"));
        }
        expected.remove(Arrays.asList("a", "1"));
        expected.remove(Arrays.asList("a", "2"));
        expected.remove(Arrays.asList("a", "3"));
        expected.remove(Arrays.asList("f", "2"));
        while (permutationGenerator.hasNext()){
            Permutation<Integer, String> permutation = permutationGenerator.next();
            List<String> strings = new ArrayList<>(2);
            strings.add(permutation.getElement(0));
            strings.add(permutation.getElement(1));
            result.add(strings);
        }
        assertEquals(expected, result);
    }

}