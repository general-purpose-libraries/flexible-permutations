package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.impl.valuedomains.SimpleValueDomain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.KeyException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by chief on 21.06.17.
 */
class FixedSizeSetPermutationGeneratorTest {
    private FixedSizeSetPermutationGenerator<Integer, String> permutationGenerator;
    private ValueDomain<String> valueDomain1;
    private ValueDomain<String> valueDomain2;

    @BeforeEach
    void setUp() {
        Collection<String> collection1 = new ArrayList<>(Arrays.asList(
                "a",
                "b",
                "c"
        ));
        Collection<String> collection2 = new ArrayList<>(Arrays.asList(
                "0",
                "1",
                "2"
        ));
        valueDomain1 = new SimpleValueDomain<>(collection1);
        valueDomain2 = new SimpleValueDomain<>(collection2);

        Map<Integer, ValueDomain<String>> valueDomainMap = new HashMap<>();
        valueDomainMap.put(0, valueDomain1);
        valueDomainMap.put(1, valueDomain2);
        permutationGenerator = new FixedSizeSetPermutationGenerator<>(valueDomainMap);
    }

    @Test
    void hasNext() {
        for (int a = 0; a < 49; a++){
            assertTrue(permutationGenerator.hasNext());
            permutationGenerator.next();
        }
        assertFalse(permutationGenerator.hasNext());
    }

    @Test
    void next() throws KeyException {
        Collection<Collection<String>> collection1 = new HashSet<>(Arrays.asList(
                Collections.singleton("a"),
                Collections.singleton("b"),
                Collections.singleton("c"),
                new HashSet<>(Arrays.asList("a", "b")),
                new HashSet<>(Arrays.asList("b", "c")),
                new HashSet<>(Arrays.asList("a", "c")),
                new HashSet<>(Arrays.asList("a", "b", "c"))
        ));
        Collection<Collection<String>> collection2 = new ArrayList<>(Arrays.asList(
                Collections.singleton("0"),
                Collections.singleton("1"),
                Collections.singleton("2"),
                new HashSet<>(Arrays.asList("0", "1")),
                new HashSet<>(Arrays.asList("1", "2")),
                new HashSet<>(Arrays.asList("0", "2")),
                new HashSet<>(Arrays.asList("0", "1", "2"))
        ));
        Collection<Set<Collection<String>>> expected = new HashSet<>();
        collection1.forEach(collection11 -> collection2.forEach(collection21 -> {
            Set<Collection<String>> row = new HashSet<>();
            row.add(collection11);
            row.add(collection21);
            expected.add(row);
        }));

        Collection<Set<Collection<String>>> result = new HashSet<>();
        while (permutationGenerator.hasNext()){
            Permutation<Integer, Collection<String>> permutation = permutationGenerator.next();
            Set<Collection<String>> strings = new HashSet<>();
            strings.add(permutation.getElement(0));
            strings.add(permutation.getElement(1));
            result.add(strings);
        }

        assertEquals(expected, result);
    }
}