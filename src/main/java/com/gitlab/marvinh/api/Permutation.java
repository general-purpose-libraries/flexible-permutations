package com.gitlab.marvinh.api;

import com.gitlab.marvinh.api.exceptions.PermutationException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * This class describes a permutation
 * @param <C> type of the columns
 * @param <V> type of the elements
 */
public interface Permutation<C, V> extends Map<C, V> {

    Class<V> getValueType();

    Class<C> getColumnType();

    /**
     * Returns the element in the given column
     * @param column Where the element should be returned
     * @return Element in the given column
     */
    V getElement(C column);

    /**
     * Returns all columns of this permutation
     */
    Collection<C> getColumns();

    @Override
    default V put(C c, V v){
        throw new UnsupportedOperationException();
    }

    @Override
    default V remove(Object o){
        throw new UnsupportedOperationException();
    }

    @Override
    default void putAll(Map<? extends C, ? extends V> map){
        throw new UnsupportedOperationException();
    }

    @Override
    default void clear(){
        throw new UnsupportedOperationException();
    }

    @Override
    default void replaceAll(BiFunction<? super C, ? super V, ? extends V> biFunction) {
        throw new UnsupportedOperationException();
    }

    @Override
    default V putIfAbsent(C c, V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean remove(Object o, Object o1) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean replace(C c, V v, V v1) {
        throw new UnsupportedOperationException();
    }

    @Override
    default V replace(C c, V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    default V get(Object o){
        if (!(o.getClass() == getColumnType().getClass()) || !containsKey(o)){
            throw new PermutationException("Object is not contained in this permutation");
        }
        return getElement((C) o);
    }

    @Override
    default Set<C> keySet(){
        return new HashSet<>(getColumns());
    }
}
