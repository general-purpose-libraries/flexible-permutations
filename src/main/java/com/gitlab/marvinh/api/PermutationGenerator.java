package com.gitlab.marvinh.api;

import java.util.Iterator;

/**
 * Generates permutations over given value domains
 */
public interface PermutationGenerator<C, V> extends Iterator<Permutation<C, V>> {

    default void remove() {
        throw new UnsupportedOperationException("'remove' is not supported for permutation generator");
    }
}
