package com.gitlab.marvinh.api.exceptions;

/**
 * Abstract class for simplifying implementation of custom RuntimeExceptions
 */
public abstract class AbstractRuntimeException extends RuntimeException {
    private final String message;

    public AbstractRuntimeException(Exception exception){
        assert exception != null;
        this.message = exception.getMessage();
        if (exception.getStackTrace().length == 0){
            this.setStackTrace(Thread.currentThread().getStackTrace());
        }
        else {
            this.setStackTrace(exception.getStackTrace());
        }
    }

    public AbstractRuntimeException(String message) {
        assert message != null;
        this.message = message;
        this.setStackTrace(Thread.currentThread().getStackTrace());
    }

    @Override
    public String toString() {
        return String.format("%s{message='%s'}", this.getClass().getSimpleName(), message);
    }
}