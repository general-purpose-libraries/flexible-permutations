package com.gitlab.marvinh.api.exceptions;

/**
 * Created by chief on 21.06.17.
 */
public class PermutationException extends AbstractRuntimeException {
    public PermutationException(Exception exception) {
        super(exception);
    }

    public PermutationException(String message) {
        super(message);
    }
}
