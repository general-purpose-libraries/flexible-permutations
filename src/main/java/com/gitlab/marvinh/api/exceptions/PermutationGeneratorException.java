package com.gitlab.marvinh.api.exceptions;

/**
 * Created by chief on 21.06.17.
 */
public class PermutationGeneratorException extends AbstractRuntimeException {
    public PermutationGeneratorException(Exception exception) {
        super(exception);
    }

    public PermutationGeneratorException(String message) {
        super(message);
    }
}
