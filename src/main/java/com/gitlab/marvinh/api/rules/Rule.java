package com.gitlab.marvinh.api.rules;

import com.gitlab.marvinh.api.Permutation;

/**
 * This interface describes rules for permutation generation
 */
public interface Rule<C, V> {
    /**
     * Checks, whether the given permutation will be accepted or not.
     * @param permutation Permutation to check
     * @return True, when it's accepted. False if not.
     */
    boolean accept(Permutation<C, V> permutation);

    /**
     * Returns whether this rule is equal to the given one.
     * @param rule Rule to check for equality with
     * @return True, if the given rule is equal to this one. False else.
     */
    boolean equals(Rule<C, V> rule);

    /**
     * @return Class ob the columns
     */
    Class<C> getColumnType();

    /**
     * @return Class of the values
     */
    Class<V> getValueType();

    /**
     * Returns whether this rule is equal to the given object.
     * This is to enforce implementation of the equals() method.
     * @param object Object to check for equality with
     * @return True, if the given object is equal to this one. False else.
     */
    boolean equals(Object object);

    /**
     * Generates a hash code for identifying this rule in a set.
     * This is to enforce implementation of the hashCode() method.
     * @return Generated hash code
     */
    int hashCode();
}
