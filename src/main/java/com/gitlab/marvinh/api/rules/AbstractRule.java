package com.gitlab.marvinh.api.rules;

/**
 * Abstract implementation of Rule for simplified
 * coding.
 */
public abstract class AbstractRule<C, V> implements Rule<C, V>{

    @Override
    public boolean equals(Object o) {
        return o instanceof Rule
                && this.getColumnType() == ((Rule) o).getColumnType()
                && this.getValueType() == ((Rule) o).getValueType()
                && equals((Rule<C, V>) o);
    }


}
