package com.gitlab.marvinh.api.rules;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.rules.misc.RuleEntry;

/**
 * Created by chief on 21.06.17.
 */
public class IfThenNotRule<C, V> extends IfThenRule<C, V> {
    public IfThenNotRule(RuleEntry<C, V> condition, RuleEntry<C, V> expectation) {
        super(condition, expectation);
    }

    @Override
    protected boolean expectationMatches(Permutation<C, V> permutation) {
        return !super.expectationMatches(permutation);
    }
}
