package com.gitlab.marvinh.api.rules;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.rules.misc.RuleEntry;

/**
 * Created by chief on 21.06.17.
 */
public class IfThenRule<C, V> extends AbstractTwoPartsRule<C, V> {

    public IfThenRule(RuleEntry<C, V> condition, RuleEntry<C, V> expectation) {
        super(condition, expectation);
    }

    @Override
    protected boolean expectationMatches(Permutation<C, V> permutation) {
        return permutation.entrySet().parallelStream().anyMatch(cvEntry -> expectation.match(cvEntry.getKey(), cvEntry.getValue()));
    }
}
