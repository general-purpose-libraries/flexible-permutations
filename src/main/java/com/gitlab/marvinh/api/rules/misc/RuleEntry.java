package com.gitlab.marvinh.api.rules.misc;

import java.util.Objects;

/**
 * Created by chief on 21.06.17.
 */
public class RuleEntry<C, V> {
    private final C column;
    private final V value;

    public RuleEntry(C column, V value) {
        assert column != null;
        assert value != null;
        this.column = column;
        this.value = value;
    }

    public boolean match(C column, V value){
        assert column != null;
        assert value != null;
        return Objects.equals(this.column, column)
                && Objects.equals(this.value, value);
    }

    public C getColumn() {
        return column;
    }

    public V getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RuleEntry<?, ?> ruleEntry = (RuleEntry<?, ?>) o;
        return Objects.equals(column, ruleEntry.column) &&
                Objects.equals(value, ruleEntry.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, value);
    }
}
