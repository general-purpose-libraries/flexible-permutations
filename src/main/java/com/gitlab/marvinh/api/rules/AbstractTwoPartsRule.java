package com.gitlab.marvinh.api.rules;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.rules.misc.RuleEntry;

import java.util.Objects;

/**
 * Created by chief on 21.06.17.
 */
public abstract class AbstractTwoPartsRule<C, V> extends AbstractRule<C, V> {
    private final RuleEntry<C, V> condition;
    protected final RuleEntry<C, V> expectation;
    private final Class<C> columnType;
    private final Class<V> valueType;

    protected AbstractTwoPartsRule(RuleEntry<C, V> condition, RuleEntry<C, V> expectation) {
        assert condition != null;
        assert expectation != null;
        columnType = (Class<C>) condition.getColumn().getClass();
        valueType = (Class<V>) condition.getValue().getClass();
        this.condition = condition;
        this.expectation = expectation;
    }

    @Override
    public Class<C> getColumnType() {
        return columnType;
    }

    @Override
    public Class<V> getValueType() {
        return valueType;
    }

    @Override
    public boolean accept(Permutation<C, V> permutation) {
        return !conditionMatches(permutation) || expectationMatches(permutation);
    }

    protected boolean conditionMatches(Permutation<C, V> permutation){
        return permutation.entrySet().parallelStream().anyMatch(cvEntry -> condition.match(cvEntry.getKey(), cvEntry.getValue()));
    }

    protected abstract boolean expectationMatches(Permutation<C, V> permutation);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractTwoPartsRule<?, ?> that = (AbstractTwoPartsRule<?, ?>) o;
        return Objects.equals(condition, that.condition) &&
                Objects.equals(expectation, that.expectation);
    }

    @Override
    public boolean equals(Rule<C, V> rule) {
        return equals((Object)rule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(condition, expectation);
    }
}
