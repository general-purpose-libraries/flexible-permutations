package com.gitlab.marvinh.api;

import java.util.Collection;

/**
 * Describes a collection of values that can be assigned
 * to a Digit. This collection is not intended to be changed
 * after creation.
 */
public interface ValueDomain<T> extends Collection<T> {
    default boolean add(Object o) {
        throw new UnsupportedOperationException("'add': This collection can not be changed");
    }

    default boolean remove(Object o) {
        throw new UnsupportedOperationException("'remove': This collection can not be changed");
    }

    default boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("'addAll': This collection can not be changed");
    }

    default void clear() {
        throw new UnsupportedOperationException("'clear': This collection can not be changed");
    }

    default boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("'retainAll': This collection can not be changed");
    }

    default boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("'removeAll': This collection can not be changed");
    }
}
