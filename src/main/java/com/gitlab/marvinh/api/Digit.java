package com.gitlab.marvinh.api;

import java.util.Iterator;

/**
 * Represents a digit in a permutation
 * @param <C> type of the columns
 * @param <V> type of the elements
 */
public interface Digit<C, V> extends Iterator<V> {
    /**
     * @return Column associated with this digit
     */
    C getColumn();

    /**
     * Reset the digit to its initial state
     */
    void reset();

    /**
     * @return Current element of the digit
     * @throws IllegalStateException If the digit has no current element
     */
    V current() throws IllegalStateException;
}
