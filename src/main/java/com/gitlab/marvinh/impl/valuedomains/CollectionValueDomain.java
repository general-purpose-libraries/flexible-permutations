package com.gitlab.marvinh.impl.valuedomains;

import java.util.Collection;

/**
 * Created by chief on 21.06.17.
 */
public class CollectionValueDomain<T> extends SimpleValueDomain<Collection<T>> {
    public CollectionValueDomain(Collection<Collection<T>> elements) {
        super(elements);
    }
}
