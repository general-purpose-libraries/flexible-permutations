package com.gitlab.marvinh.impl.valuedomains;

import com.gitlab.marvinh.api.ValueDomain;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;

/**
 * This class is a basic implementation of a value domain
 */
public class SimpleValueDomain<T> implements ValueDomain<T> {
    private final Collection<T> elements;

    public SimpleValueDomain(Collection<T> elements) {
        assert elements != null;
        if (elements.isEmpty()){
            throw new IllegalArgumentException("Value domain can not be empty");
        }
        this.elements = new LinkedList<>(elements);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return elements.toArray(objects);
    }

    @Override
    public boolean containsAll(Collection collection) {
        return elements.containsAll(collection);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleValueDomain<?> that = (SimpleValueDomain<?>) o;
        return Objects.equals(elements, that.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "elements=" + elements +
                '}';
    }
}
