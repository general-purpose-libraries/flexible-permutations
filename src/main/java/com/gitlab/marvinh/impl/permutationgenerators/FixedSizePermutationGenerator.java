package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.Digit;
import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.api.rules.Rule;
import com.gitlab.marvinh.impl.utils.DefaultDigit;
import com.gitlab.marvinh.impl.utils.SimplePermutation;

import java.util.*;

/**
 * Generates permutations of a fixed size. The size of the permutations is equal
 * to the amount of columns
 */
public class FixedSizePermutationGenerator<C, V> extends AbstractPermutationGenerator<C, V> {
    private final List<Digit<C, V>> permutation = new LinkedList<>();

    protected boolean isFinished;

    protected FixedSizePermutationGenerator(){};

    public FixedSizePermutationGenerator(Map<C, ValueDomain<V>> valueDomains) {
        init(valueDomains, Collections.emptySet());
    }

    public FixedSizePermutationGenerator(Map<C, ValueDomain<V>> valueDomains, Collection<Rule<C, V>> rules) {
        init(valueDomains, rules);
    }

    protected void init(Map<C, ValueDomain<V>> valueDomains, Collection<Rule<C, V>> rules){
        if (valueDomains == null) {
            throw new NullPointerException("Parameter 'valueDomains' is null");
        }
        if (valueDomains.isEmpty()) {
            throw new IllegalArgumentException("Empty set of value domains is not supported");
        }

        for (C column : valueDomains.keySet()) {
            Digit<C, V> digit = new DefaultDigit<>(column, valueDomains.get(column));
            permutation.add(digit);
        }
        isFinished = false;
        if (rules == null){
            throw new NullPointerException("Parameter 'rules' is null");
        }
        this.rules.addAll(rules);
    }

    public Optional<Permutation<C, V>> generate() {
        if (isFinished){
            return Optional.empty();
        }

        boolean move = true;
        Map<C, V> elements = new HashMap<>();
        Iterator<Digit<C, V>> iterator = permutation.iterator();
        while (iterator.hasNext()) {
            Digit<C, V> digit = iterator.next();
            elements.put(digit.getColumn(), digit.current());
            if (move){
                if (digit.hasNext()){
                    digit.next();
                    move = false;
                }
                else {
                    digit.reset();
                    if (!iterator.hasNext()){
                        isFinished = true;
                    }
                }
            }
        }

        return Optional.of(new SimplePermutation<>(elements));
    }
}
