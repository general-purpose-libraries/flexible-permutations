package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.api.rules.Rule;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by chief on 26.06.17.
 */
public class FixedSizeSetWithEmptyPermutationGenerator<C, V> extends FixedSizeSetPermutationGenerator<C, V> {
    public FixedSizeSetWithEmptyPermutationGenerator(Map<C, ValueDomain<V>> valueDomains) {
        super(valueDomains);
    }

    public FixedSizeSetWithEmptyPermutationGenerator(Map<C, ValueDomain<V>> valueDomains, Collection<Rule<C, Collection<V>>> rules) {
        super(valueDomains, rules);
    }

    @Override
    protected Collection<Collection<V>> getValueCollections(ValueDomain<V> valueDomain) {
        Collection<Collection<V>> valueCollections = super.getValueCollections(valueDomain);
        valueCollections.add(Collections.emptySet());
        return valueCollections;
    }
}
