package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.PermutationGenerator;
import com.gitlab.marvinh.api.exceptions.PermutationGeneratorException;
import com.gitlab.marvinh.api.rules.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

/**
 * This class is a base class of all permutation generators
 * and is used to simplify implementation.
 */
public abstract class AbstractPermutationGenerator<C, V> implements PermutationGenerator<C, V> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPermutationGenerator.class);

    private Optional<Permutation<C, V>> nextPermutation = Optional.empty();
    protected final Collection<Rule<C, V>> rules = new HashSet<>();

    protected AbstractPermutationGenerator(){}

    protected AbstractPermutationGenerator(Collection<Rule<C, V>> rules) {
        assert rules != null;
        this.rules.addAll(rules);
    }

    @Override
    public boolean hasNext() {
        nextPermutation = generateNextPermutation();
        return nextPermutation.isPresent();
    }

    @Override
    public Permutation<C, V> next() {
        Optional<Permutation<C, V>> result = nextPermutation;
        if (!result.isPresent()){
            nextPermutation = generateNextPermutation();
            if (!nextPermutation.isPresent()){
                throw new PermutationGeneratorException("No next permutation.");
            }
        }
        nextPermutation = Optional.empty();
        assert result.isPresent();
        LOGGER.debug(String.format("Permutation generated: %s", result.get()));
        return result.get();
    }

    private Optional<Permutation<C, V>> generateNextPermutation(){
        Optional<Permutation<C, V>> permutation;
        do {
            permutation = generate();
            if (!permutation.isPresent()){
                return Optional.empty();
            }
        } while (!permutationIsAccepted(permutation.get()));
        return permutation;
    }

    /**
     * Checks whether a given permutation is accepted by this
     * generator. This is, when all rules accept the permutation.
     * @param permutation Permutation to check.
     * @return True, if it is accepted. False else.
     */
    private boolean permutationIsAccepted(Permutation<C, V> permutation){
        assert permutation != null;
        for (Rule<C, V> rule : rules){
            if (!rule.accept(permutation)){
                return false;
            }
        }
        return true;
    }

    protected abstract Optional<Permutation<C, V>> generate();
}
