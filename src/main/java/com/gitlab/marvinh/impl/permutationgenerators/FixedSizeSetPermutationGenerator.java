package com.gitlab.marvinh.impl.permutationgenerators;

import com.gitlab.marvinh.api.ValueDomain;
import com.gitlab.marvinh.api.rules.Rule;
import com.gitlab.marvinh.impl.valuedomains.CollectionValueDomain;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by chief on 27.05.17.
 */
public class FixedSizeSetPermutationGenerator<C, V> extends FixedSizePermutationGenerator<C, Collection<V>> {
    private static class SuperSetComputationRowSet<V> extends AbstractMap<V, V>{
        private final Collection<SuperSetComputationRow<V>> rows;

        private SuperSetComputationRowSet(Collection<SuperSetComputationRow<V>> rows) {
            assert rows != null;
            this.rows = new HashSet<>(rows);
        }

        private static <V> SuperSetComputationRowSet<Collection<V>> rowSetOf(Collection<Collection<V>> elements) {
            assert elements != null;
            assert !elements.isEmpty();

            Collection<SuperSetComputationRow<Collection<V>>> rows = new HashSet<>();

            elements.parallelStream().forEach(entryA -> elements.parallelStream().forEach(entryB -> {
                if (Objects.equals(entryA, entryB)){
                    return;
                }
                SuperSetComputationRow<Collection<V>> row = new SuperSetComputationRow<>(entryA, entryB);
                rows.add(row);
            }));

            return new SuperSetComputationRowSet<>(rows);
        }

        @Override
        public Set<Entry<V, V>> entrySet() {
            return new HashSet<>(rows);
        }
    }

    private static class SuperSetComputationRow<V> implements Map.Entry<V, V>{
        private final V entryA;
        private final V entryB;

        private SuperSetComputationRow(V entryA, V entryB) {
            assert entryA != null;
            assert entryB != null;
            this.entryA = entryA;
            this.entryB = entryB;
        }

        @Override
        public V getKey() {
            return entryA;
        }

        @Override
        public V getValue() {
            return entryB;
        }

        @Override
        public V setValue(V v) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SuperSetComputationRow<?> that = (SuperSetComputationRow<?>) o;
            return Objects.equals(entryA, that.entryA) && Objects.equals(entryB, that.entryB)
                    || Objects.equals(entryA, that.entryB) && Objects.equals(entryB, that.entryA);
        }

        @Override
        public int hashCode() {
            return entryA.hashCode() ^ entryB.hashCode();
        }

        @Override
        public String toString() {
            return "SuperSetComputationRow{"
                     + entryA +
                    ", " + entryB +
                    '}';
        }
    }

    public FixedSizeSetPermutationGenerator(Map<C, ValueDomain<V>> valueDomains) {
        init(computeSubSets(valueDomains), Collections.emptySet());
    }

    public FixedSizeSetPermutationGenerator(Map<C, ValueDomain<V>> valueDomains, Collection<Rule<C, Collection<V>>> rules) {
        init(computeSubSets(valueDomains), rules);
    }

    private Map<C, ValueDomain<Collection<V>>> computeSubSets(Map<C, ValueDomain<V>> valueDomains) {
        Map<C, ValueDomain<Collection<V>>> result = new HashMap<>();

        valueDomains.forEach((key, valueDomain) -> result.put(key, computeSubSets(valueDomain)));

        return result;
    }

    private ValueDomain<Collection<V>> computeSubSets(ValueDomain<V> valueDomain) {
        Collection<Collection<V>> elements = getValueCollections(valueDomain);

        return new CollectionValueDomain<>(elements);
    }

    protected Collection<Collection<V>> getValueCollections(ValueDomain<V> valueDomain) {
        Collection<Collection<V>> elements = new HashSet<>();
        valueDomain.forEach(value -> elements.add(Collections.singleton(value)));

        SuperSetComputationRowSet<Collection<V>> nextSuperSets = SuperSetComputationRowSet.rowSetOf(elements);
        while (!nextSuperSets.isEmpty()){
            Collection<Collection<V>> superSets = computeNextSuperSet(nextSuperSets);
            elements.addAll(superSets);
            nextSuperSets = SuperSetComputationRowSet.rowSetOf(superSets);
        }
        return elements;
    }

    private Collection<Collection<V>> computeNextSuperSet(SuperSetComputationRowSet<Collection<V>> nextSuperSets) {
        Collection<Collection<V>> rows = new ConcurrentSkipListSet<>(new Comparator<Collection<V>>() {
            @Override
            public int compare(Collection<V> vs, Collection<V> t1) {
                int compare = Integer.compare(vs.hashCode(), t1.hashCode());
                if (compare != 0){
                    return compare;
                }
                if (Objects.equals(vs, t1)){
                    return 0;
                }
                return -1;
            }
        });

        nextSuperSets.entrySet().parallelStream().forEach(collectionCollectionEntry -> {
            assert collectionCollectionEntry instanceof SuperSetComputationRow;
            Collection<V> newSet = new HashSet<>(collectionCollectionEntry.getKey());
            newSet.addAll(collectionCollectionEntry.getValue());
            rows.add(newSet);
        });

        return rows;
    }
}
