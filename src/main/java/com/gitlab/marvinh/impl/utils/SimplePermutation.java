package com.gitlab.marvinh.impl.utils;

import com.gitlab.marvinh.api.Permutation;
import com.gitlab.marvinh.api.exceptions.PermutationException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class is a basic implementation of a permutation
 */
public class SimplePermutation<C, V> implements Permutation<C, V> {
    private final Map<C, V> elements;
    private final Class<C> columnType;
    private final Class<V> valueType;

    public SimplePermutation(Map<C, V> elements) {
        assert elements != null;
        assert !elements.isEmpty();
        this.elements = new HashMap<>(elements);
        Entry<C, V> entry = elements.entrySet().iterator().next();
        columnType = (Class<C>) entry.getKey().getClass();
        valueType = (Class<V>) entry.getValue().getClass();
    }

    @Override
    public Class<V> getValueType() {
        return valueType;
    }

    @Override
    public Class<C> getColumnType() {
        return columnType;
    }

    @Override
    public V getElement(C column) {
        if (!elements.containsKey(column)){
            throw new PermutationException(String.format("Key '%s' does not exist", column));
        }
        return elements.get(column);
    }

    @Override
    public Collection<C> getColumns() {
        return elements.keySet();
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return elements.containsKey(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return elements.containsValue(o);
    }

    @Override
    public Collection<V> values() {
        return elements.values();
    }

    @Override
    public Set<Entry<C, V>> entrySet() {
        return elements.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimplePermutation<?, ?> that = (SimplePermutation<?, ?>) o;

        return elements.equals(that.elements);
    }

    @Override
    public int hashCode() {
        return elements.hashCode();
    }

    @Override
    public String toString() {
        return "SimplePermutation{" +
                "elements=" + elements +
                '}';
    }
}
