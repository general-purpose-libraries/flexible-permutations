package com.gitlab.marvinh.impl.utils;

import com.gitlab.marvinh.api.Digit;
import com.gitlab.marvinh.api.ValueDomain;

import java.util.Iterator;

/**
 * Default implementation of a digit in the permutation generators
 */
public class DefaultDigit<C, V> implements Digit<C, V> {
    private final C column;
    private final ValueDomain<V> valueDomain;

    private Iterator<V> iterator;
    private V current;

    public DefaultDigit(C column, ValueDomain<V> valueDomain) {
        assert column != null;
        assert valueDomain != null;
        this.column = column;
        this.valueDomain = valueDomain;
        this.iterator = valueDomain.iterator();
        current = iterator.next();
    }

    @Override
    public C getColumn() {
        return column;
    }

    @Override
    public void reset() {
        iterator = valueDomain.iterator();
        current = iterator.next();
    }

    @Override
    public V current() throws IllegalStateException {
        assert current != null;
        return current;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public V next() {
        if (!iterator.hasNext()){
            throw new IllegalStateException("Digit has no next element");
        }
        current = iterator.next();
        return current;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DefaultDigit<?, ?> that = (DefaultDigit<?, ?>) o;

        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        return valueDomain != null ? valueDomain.equals(that.valueDomain) : that.valueDomain == null;
    }

    @Override
    public int hashCode() {
        int result = column != null ? column.hashCode() : 0;
        result = 31 * result + (valueDomain != null ? valueDomain.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DefaultDigit{" +
                "column=" + column +
                ", valueDomain=" + valueDomain +
                '}';
    }
}
